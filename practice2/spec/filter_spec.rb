require "filter"

describe "filter" do
  before(:each) {@nums= [6, -5, 319, 400, 18, 94, 11] }

  it "should yield the original array", pending: true do
    expect { |b| filter(@nums, {}, &b) }.to yield_successive_args(6, -5, 319, 400, 18, 94, 11)
  end

  it "should yield -5", pending: true do
    expect { |b| filter(@nums,:max => 0, &b) }.to yield_successive_args(-5)
  end

  it "should yield 628 and 22", pending: true do
     expect { |b| filter(@nums,:min => 10, :max => 350, :odd => 1, :scale => 2, &b) }.to yield_successive_args(638,22)
  end

  it "should not modify original array", pending: true do
    expect { |b| filter(@nums,:max => 0, :scale => -2, &b) }.to yield_successive_args(10)
    expect { |b| filter(@nums,:max => 0, :scale => -2, &b) }.to yield_successive_args(10)
  end

  it "should check that a number is less than max/more than min before scaling", pending: true do
    expect { |b| filter(@nums,:scale => 4, :min => -10, :max => 20, &b) }.to yield_successive_args(24, -20, 72, 44)
  end
end
